var projects = document.getElementsByClassName("hex");

var doc = document.getElementsByTagName("html")[0];
var wrap = document.getElementById("body");

var modal_bg = document.getElementById("modal_bg");
var modal_title = document.getElementById("modal_title");
var modal_sub = document.getElementById("modal_sub");
var modal_content = document.getElementById("modal_content");
var modal_close = document.getElementById("modal_close");
var modal = document.getElementById("modal");

var project_content = document.getElementById("content");

var scrollTop;

function openModal(project) {
  const scrollBarWidth = window.innerWidth - document.documentElement.clientWidth;
  project_content.style.paddingRight = `${scrollBarWidth}px`;
  modal.style.paddingRight = '0'
  scrollTop = window.pageYOffset;
  doc.className = "modal_open";
  wrap.style.top = scrollTop * -1 + 'px';
  modal_title.innerText = project.dataset.title;
  modal_sub.innerText = project.dataset.sub;
  var content = JSON.parse(project.dataset.content);
  for (var i = 0; i < content.length; i++) {
    if (content[i].type === "image") {
      var img = new Image();
      img.src = content[i].src;
      modal_content.appendChild(img);
    } else if (content[i].type === "paragraph") {
      var p = document.createElement("p");
      p.innerText = content[i].text;
      modal_content.appendChild(p);
    }
  }
}

function closeModal() {
  project_content.style.paddingRight = '0';
  modal.style.paddingRight = '15px';
  doc.className = ""
  wrap.style.top = '';
  window.scrollTo(0, scrollTop);
  document.body.style.paddingRight = '';
  window.setTimeout(function () {
    modal_content.innerHTML = "";
    scrollTop = null;
  }, 400)
}

window.onload = function () {
  for (var i = 0; i < projects.length; i++) {
    var project = projects[i];
    project.onclick = function () {
      openModal(this)
    }
  }
  modal_bg.onclick = function () {
    closeModal()
  }
  modal_close.onclick = function () {
    closeModal()
  }
}
document.onkeydown = function (evt) {
  evt = evt || window.event;
  var isEscape = false;
  if ("key" in evt) {
    isEscape = (evt.key == "Escape" || evt.key == "Esc");
  } else {
    isEscape = (evt.keyCode == 27);
  }
  if (isEscape) {
    closeModal()
  }
};
